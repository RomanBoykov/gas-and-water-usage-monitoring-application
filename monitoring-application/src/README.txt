The project is launched with a built-in database and is ready to work immediately after launch

Creating executable jar using maven:
 - mvn clean package
Executing jar file:
- project_directory:> java -jar target/monitoring-application-0.0.1-SNAPSHOT.jar


Port: 3489
Swagger: http://localhost:3489/swagger-ui/index.html