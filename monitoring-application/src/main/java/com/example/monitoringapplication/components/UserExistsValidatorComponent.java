package com.example.monitoringapplication.components;

import com.example.monitoringapplication.models.annotations.UserValidate;
import com.example.monitoringapplication.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;


@Component
@RequiredArgsConstructor
public class UserExistsValidatorComponent implements ConstraintValidator<UserValidate, UUID> {

    private final UserService userService;

    @Override
    public boolean isValid(UUID userId, ConstraintValidatorContext context) {
        return userId != null && userService.isUserExist(userId);
    }

}
