package com.example.monitoringapplication.controllers;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.entity.UserAccount;
import com.example.monitoringapplication.mappers.EntityMapper;
import com.example.monitoringapplication.models.annotations.UserValidate;
import com.example.monitoringapplication.models.requests.UserMeterReadingsRequestDto;
import com.example.monitoringapplication.models.responses.MeterReadingsResponseDto;
import com.example.monitoringapplication.services.MeterReadingsService;
import com.example.monitoringapplication.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/readings")
@Tag(name = "Readings of water and gas sensors",
        description = "It is possible to transfer utility data on water and gas to this controller for a specific user," +
                " as well as receive data history")
@RequiredArgsConstructor
@Validated
public class UserMeterReadingsController {

    private final EntityMapper mapper;
    private final MeterReadingsService meterReadingsService;
    private final UserService userService;

    @Operation(summary = "Get user's meter history")
    @GetMapping("/{userId}")
    public List<MeterReadingsResponseDto> getUserMeterReadingsHistory(
            Pageable pageable,
            @PathVariable @UserValidate UUID userId) {
        Page<MeterData> meterDataPage = meterReadingsService.getUserMeterReadingsHistory(userId, pageable);
        if (CollectionUtils.isEmpty(meterDataPage.getContent())) {
            return new ArrayList<>();
        }
        return mapper.toMeterReadingsResponseDtoList(meterDataPage.getContent());
    }

    @Operation(summary = "Add user utility meter readings")
    @PostMapping("/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void submitUserMeterReadings(@RequestBody @Valid UserMeterReadingsRequestDto userMeterReadingsRequestDto,
                                        @PathVariable @UserValidate UUID userId) {
        UserAccount userAccount = userService.getByIdOrThrow(userId);
        meterReadingsService.createUserMeterReadings(userAccount, userMeterReadingsRequestDto);
    }
}
