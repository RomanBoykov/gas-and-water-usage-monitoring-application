package com.example.monitoringapplication.mappers;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.models.requests.MeterReadingsDto;
import com.example.monitoringapplication.models.responses.MeterReadingsResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EntityMapper {
    List<MeterReadingsResponseDto> toMeterReadingsResponseDtoList(List<MeterData> meterDataList);

    MeterData toMeterData(MeterReadingsDto meterReadingsDto);
}
