package com.example.monitoringapplication.services;

import com.example.monitoringapplication.entity.UserAccount;
import lombok.NonNull;

import java.util.UUID;

public interface UserService {
    boolean isUserExist(UUID userId);

    UserAccount getByIdOrThrow(@NonNull UUID userId);
}
