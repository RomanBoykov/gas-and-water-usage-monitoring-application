package com.example.monitoringapplication.services.impl;

import com.example.monitoringapplication.entity.UserAccount;
import com.example.monitoringapplication.models.exceptions.NotFoundException;
import com.example.monitoringapplication.repository.UserAccountRepository;
import com.example.monitoringapplication.services.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("userService")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserAccountRepository userAccountRepository;

    @Override
    public boolean isUserExist(UUID userId) {
        return userAccountRepository.existsById(userId);
    }

    @Override
    public UserAccount getByIdOrThrow(@NonNull UUID userId) {
        return userAccountRepository.findById(userId).orElseThrow(NotFoundException::new);
    }
}
