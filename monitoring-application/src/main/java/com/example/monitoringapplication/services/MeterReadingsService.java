package com.example.monitoringapplication.services;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.entity.UserAccount;
import com.example.monitoringapplication.models.requests.UserMeterReadingsRequestDto;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface MeterReadingsService {
    Page<MeterData> getUserMeterReadingsHistory(@NonNull UUID userId, @NonNull Pageable pageable);

    void createUserMeterReadings(@NonNull UserAccount userAccount, @NonNull UserMeterReadingsRequestDto userMeterReadingsRequestDto);
}
