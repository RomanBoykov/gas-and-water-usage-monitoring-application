package com.example.monitoringapplication.services.impl;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.entity.UserAccount;
import com.example.monitoringapplication.mappers.EntityMapper;
import com.example.monitoringapplication.models.exceptions.NotValidMeterDataException;
import com.example.monitoringapplication.models.requests.MeterReadingsDto;
import com.example.monitoringapplication.models.requests.UserMeterReadingsRequestDto;
import com.example.monitoringapplication.repository.MeterDataRepository;
import com.example.monitoringapplication.services.MeterReadingsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MeterReadingsServiceImpl implements MeterReadingsService {

    private final MeterDataRepository meterDataRepository;
    private final EntityMapper entityMapper;

    @Override
    public Page<MeterData> getUserMeterReadingsHistory(@NonNull UUID userId,
                                                       @NonNull Pageable pageable) {
        return meterDataRepository.findAllByUserId(userId, pageable);
    }

    @Override
    @Transactional
    //Perhaps if the sensor data is submitted all at once,
    // then you need to cancel saving them to the database if at least one of the parameters is not correct
    public void createUserMeterReadings(@NonNull UserAccount userAccount,
                                        @NonNull UserMeterReadingsRequestDto userMeterReadingsRequestDto) {
        List<MeterReadingsDto> meterReadingsDtoList = userMeterReadingsRequestDto.getMeterReadingsDtoList();
        validateMeterReadingsList(meterReadingsDtoList);
        meterReadingsDtoList.forEach(meterReadingsDto -> {
            validateMeterReadings(userAccount.getId(), meterReadingsDto);
            MeterData meterData = entityMapper.toMeterData(meterReadingsDto);
            meterData.setUserAccount(userAccount);
            save(meterData);
        });
    }

    private void validateMeterReadingsList(List<MeterReadingsDto> meterReadingsDtoList) {
        long distinctTypes = meterReadingsDtoList
                .stream()
                .map(MeterReadingsDto::getMeasurementType)
                .distinct()
                .count();
        if (distinctTypes != meterReadingsDtoList.size()) {
            throw new NotValidMeterDataException();
        }
    }

    private void validateMeterReadings(UUID userId, MeterReadingsDto meterReadingsDto) {
        Optional<MeterData> optionalMeterData = meterDataRepository.findByUserIdAndMeasurementTypeAndMeterReadingGreaterThanEqual(
                userId,
                meterReadingsDto.getMeasurementType(),
                meterReadingsDto.getMeterReading());
        if (optionalMeterData.isPresent()) {
            throw new NotValidMeterDataException();
        }
    }

    private MeterData save(MeterData meterData) {
        return meterDataRepository.save(meterData);
    }
}
