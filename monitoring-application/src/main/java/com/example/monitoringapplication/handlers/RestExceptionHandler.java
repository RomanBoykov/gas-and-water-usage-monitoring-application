package com.example.monitoringapplication.handlers;

import com.example.monitoringapplication.models.exceptions.NotFoundException;
import com.example.monitoringapplication.models.exceptions.NotValidMeterDataException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> handleConflict(NotFoundException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(buildResponseMap(exception));
    }

    @ExceptionHandler(value = NotValidMeterDataException.class)
    public ResponseEntity<Object> handleConflict(NotValidMeterDataException exception) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(buildResponseMap(exception));
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleConflict(MethodArgumentNotValidException exception) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(buildResponseMap(exception));
    }

    private Map<String, Object> buildResponseMap(Exception exception) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("timestamp", LocalDateTime.now());
        responseMap.put("message", exception.getMessage());
        return responseMap;
    }
}
