package com.example.monitoringapplication.entity;

import com.example.monitoringapplication.models.enums.MeasurementType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "meters_data")
public class MeterData extends Base {

    @Column(name = "meter_reading", nullable = false)
    private Double meterReading;

    @Column(name = "measurement_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private MeasurementType measurementType;

    @Column(name = "user_account_id", nullable = false, insertable = false, updatable = false)
    private UUID userId;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_account_id", referencedColumnName = "id")
    private UserAccount userAccount;

}
