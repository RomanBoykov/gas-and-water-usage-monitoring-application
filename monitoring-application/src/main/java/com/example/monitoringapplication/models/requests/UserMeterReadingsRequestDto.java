package com.example.monitoringapplication.models.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Request to save the user's utility meter data")
public class UserMeterReadingsRequestDto {

    @NotEmpty
    @Valid
    @Schema(description = "List of user sensor readings. Each list entry must have a unique type of sensor readings")
    List<MeterReadingsDto> meterReadingsDtoList;
}
