package com.example.monitoringapplication.models.requests;

import com.example.monitoringapplication.models.enums.MeasurementType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class MeterReadingsDto {

    @PositiveOrZero
    @Schema(description = "Meter readings")
    private Double meterReading;

    @NotNull
    @Schema(description = "Meter reading type")
    private MeasurementType measurementType;
}
