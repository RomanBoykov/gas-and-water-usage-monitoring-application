package com.example.monitoringapplication.models.responses;

import com.example.monitoringapplication.models.enums.MeasurementType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "History of user utility indicators")
public class MeterReadingsResponseDto {

    @NotNull
    @Schema(description = "Record creation time")
    private LocalDateTime createdAt;

    @NotNull
    @Schema(description = "Meter readings")
    private Double meterReading;

    @NotNull
    @Schema(description = "Meter reading type")
    private MeasurementType measurementType;
}
