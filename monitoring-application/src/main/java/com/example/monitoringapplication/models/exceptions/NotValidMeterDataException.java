package com.example.monitoringapplication.models.exceptions;

public class NotValidMeterDataException extends RuntimeException {

    public NotValidMeterDataException() {
        super("Not valid meter data");
    }

    public NotValidMeterDataException(String errorMessage) {
        super(errorMessage);
    }
}