package com.example.monitoringapplication.models.annotations;

import com.example.monitoringapplication.components.UserExistsValidatorComponent;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Throw exception if user not valid
 */
@Constraint(validatedBy = UserExistsValidatorComponent.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserValidate {

    String message() default "User not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
