package com.example.monitoringapplication.models.enums;

public enum MeasurementType {
    WATER, GAZ, HOT_WATER
}
