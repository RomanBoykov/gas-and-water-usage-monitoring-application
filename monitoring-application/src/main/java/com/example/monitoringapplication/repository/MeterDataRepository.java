package com.example.monitoringapplication.repository;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.models.enums.MeasurementType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MeterDataRepository extends JpaRepository<MeterData, UUID> {

    Page<MeterData> findAllByUserId(UUID userId, Pageable pageable);

    Optional<MeterData> findByUserIdAndMeasurementTypeAndMeterReadingGreaterThanEqual(UUID userId,
                                                                                      MeasurementType measurementType,
                                                                                      Double meterReading);
}
