package com.example.monitoringapplication.services.impl;

import com.example.monitoringapplication.entity.MeterData;
import com.example.monitoringapplication.entity.UserAccount;
import com.example.monitoringapplication.models.enums.MeasurementType;
import com.example.monitoringapplication.models.requests.MeterReadingsDto;
import com.example.monitoringapplication.models.requests.UserMeterReadingsRequestDto;
import com.example.monitoringapplication.services.MeterReadingsService;
import com.example.monitoringapplication.services.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class MeterReadingsServiceImplTest {

    private final UUID userId = UUID.fromString("552ec6ec-239a-11eb-adfe-f73bcf72dc9b");

    @Autowired
    MeterReadingsService meterReadingsService;
    @Autowired
    UserService userService;


    @Test
    @Transactional
    void testCreateRead() {
        UserAccount userAccount = userService.getByIdOrThrow(userId);
        List<MeterReadingsDto> meterReadingsDtoList = new ArrayList<>();
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.GAZ));
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.WATER));
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.HOT_WATER));
        meterReadingsService.createUserMeterReadings(userAccount, UserMeterReadingsRequestDto
                .builder()
                .meterReadingsDtoList(meterReadingsDtoList)
                .build());
        Page<MeterData> userMeterReadingsHistory = meterReadingsService.getUserMeterReadingsHistory(userId, Pageable.ofSize(10));

        Assertions.assertThat(userMeterReadingsHistory.getContent().size()).isEqualTo(3);
    }

}