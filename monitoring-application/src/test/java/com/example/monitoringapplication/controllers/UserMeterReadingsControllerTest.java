package com.example.monitoringapplication.controllers;

import com.example.monitoringapplication.models.enums.MeasurementType;
import com.example.monitoringapplication.models.exceptions.NotValidMeterDataException;
import com.example.monitoringapplication.models.requests.MeterReadingsDto;
import com.example.monitoringapplication.models.requests.UserMeterReadingsRequestDto;
import com.example.monitoringapplication.models.responses.MeterReadingsResponseDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserMeterReadingsControllerTest {

    private final UUID userId = UUID.fromString("552ec6ec-239a-11eb-adfe-f73bcf72dc9b");
    @Autowired
    UserMeterReadingsController userMeterReadingsController;

    @Test
    @Transactional
    void testCreateRead() {
        List<MeterReadingsDto> meterReadingsDtoList = new ArrayList<>();
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.GAZ));
        userMeterReadingsController.submitUserMeterReadings(UserMeterReadingsRequestDto
                .builder()
                .meterReadingsDtoList(meterReadingsDtoList)
                .build(), userId);
        List<MeterReadingsResponseDto> userMeterReadingsHistory = userMeterReadingsController
                .getUserMeterReadingsHistory(Pageable.ofSize(10), userId);

        Assertions.assertThat(userMeterReadingsHistory.size()).isEqualTo(1);
    }

    @Test
    public void errorHandlingDuplicateValidationExceptionThrown() {
        List<MeterReadingsDto> meterReadingsDtoList = new ArrayList<>();
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.GAZ));
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.GAZ));

        Assertions.assertThatExceptionOfType(NotValidMeterDataException.class)
                .isThrownBy(() -> userMeterReadingsController.submitUserMeterReadings(UserMeterReadingsRequestDto
                        .builder()
                        .meterReadingsDtoList(meterReadingsDtoList)
                        .build(), userId));
    }

    @Test
    @Transactional
    public void errorHandlingIncorrectMetricsValidationExceptionThrown() {
        List<MeterReadingsDto> meterReadingsDtoList = new ArrayList<>();
        meterReadingsDtoList.add(new MeterReadingsDto(4.5, MeasurementType.GAZ));
        userMeterReadingsController.submitUserMeterReadings(UserMeterReadingsRequestDto
                .builder()
                .meterReadingsDtoList(meterReadingsDtoList)
                .build(), userId);
        meterReadingsDtoList.clear();
        meterReadingsDtoList.add(new MeterReadingsDto(3.5, MeasurementType.GAZ));

        Assertions.assertThatExceptionOfType(NotValidMeterDataException.class)
                .isThrownBy(() -> userMeterReadingsController.submitUserMeterReadings(UserMeterReadingsRequestDto
                        .builder()
                        .meterReadingsDtoList(meterReadingsDtoList)
                        .build(), userId));
    }
}